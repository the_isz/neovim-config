colorscheme wombat
" make matching parens more visible with wombat scheme
hi MatchParen guifg=#e5786d guibg=NONE

set autochdir
set number
set hidden
set colorcolumn=+1
set mouse=a

" only look for files in the current directory
set path=,,

" put the backup files in the same subdirectory as the swap files, which are
" put into $XDG_DATA_HOME/nvim/swap by default.
if exists("$XDG_DATA_HOME")
    set backupdir=$XDG_DATA_HOME/nvim/backup
else
    set backupdir=$HOME/.local/share/nvim/backup
endif

" switch to the previous window similar to screen
nnoremap <c-w><c-w> <c-w>p

" combine redisplay and highlight removal
nnoremap <silent> <C-l> :nohl<CR><C-l>

" just a little typing saver...
cnoremap vrs vertical rightbelow sfind

" Open a temporary buffer
command TempBuf exe '<mods> new | setlocal buftype=nofile tw=0'

" Keep NERD_commenter from removing extra spaces (screws up indentation).
let NERDRemoveExtraSpaces=0

for plugin in ["UltiSnips", "NERD_commenter", "Tabular"]
    " :set doesn't support expressions and :let doesn't support appending to an
    " option with += (we could use string concatenation, though)
    exe "set runtimepath+=" . expand("<sfile>:p:h") . "/scripts/" . plugin
endfor

" Put these in an autocmd group, so that we can delete them easily.
augroup init_vim
au!

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
" Also don't do it when the mark is in the first line, that is the default
" position when opening a file.
autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif

let g:xml_syntax_folding=1
autocmd FileType xml setlocal foldmethod=syntax
autocmd BufRead,BufNewFile *.tex setfiletype tex
autocmd FileType tex setlocal sw=4 et tw=80

augroup END
