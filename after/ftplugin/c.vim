" vim: set et sw=2:

set cindent shiftwidth=4

set textwidth=80

" don't use tabs at all
set expandtab

" function switching between source and header files
nnoremap <buffer> <F4> :call EditOther()<CR>
" The test whether the function already exists is necessary because the :edit
" at the end will source this file again (through the ftplugin mechanism),
" thus trying to redefine the function which is currently running, yielding an
" error.
if !exists("*EditOther")
  function! EditOther()
    " Current file name
    let l:file_name = @%
    " Current file's name with "xx" or "pp" removed
    let l:base_name = ""
    " Name of new file to create if we have no file to switch to
    let l:new_file  = ""

    if l:file_name =~ '\.c\(xx\|pp\)\?$'
      let l:base_name = fnamemodify(l:file_name, ':p:s/c\(xx\|pp\)\?$/h/')
      let l:new_file  = fnamemodify(l:file_name, ':p:s/c\(xx\|pp\)\?$/h\1/')
    elseif l:file_name =~ '\.h\(xx\|pp\)\?$'
      let l:base_name = fnamemodify(l:file_name, ':p:s/h\(xx\|pp\)\?$/c/')
      let l:new_file  = fnamemodify(l:file_name, ':p:s/h\(xx\|pp\)\?$/c\1/')
    else
      echo "Unknown file pattern"
      return
    endif

    " FIXME: It would be best to try the file with the correct suffix first
    for name in [l:base_name, l:base_name . "xx", l:base_name . "pp"]
      if filereadable(name)
        exe "find" fnameescape(name)
        return
      endif
    endfor

    let l:choice = confirm("\"" . l:new_file . "\"" . " does not exist. Create?", "&yes\n&no", 0, "Question")
    if 1 == l:choice
      exe "edit" fnameescape(l:new_file)
    endif

  endfunction
endif

" function converting c++ function declarations to implementations
nnoremap <buffer> <Leader>i :call ToImpl()<CR>
function! ToImpl()
  s/virtual//e
  s/static//e
  s/friend//e
  normal ==^f(Bh
  let l:className = ""
  if exists("g:toImplClassName")
     let l:className = g:toImplClassName
  endif
  call inputsave()
  let g:toImplClassName = input("Class name: ",l:className)
  call inputrestore()
  exe "normal i\r" . g:toImplClassName . "::\ef(%$s\r{\r}\r\ej"
endfunction
