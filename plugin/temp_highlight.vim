" vim: set sw=4 et:

if exists("g:temp_highlight_loaded")
    finish
endif

let g:temp_highlight_loaded = 1

" Create highlight groups

" gui=[none,bold,italic]

" Suggested color names (these are available on most systems):
"     Red	LightRed	DarkRed
"     Green	LightGreen	DarkGreen	SeaGreen
"     Blue	LightBlue	DarkBlue	SlateBlue
"     Cyan	LightCyan	DarkCyan
"     Magenta	LightMagenta	DarkMagenta
"     Yellow	LightYellow	Brown		DarkYellow
"     Gray	LightGray	DarkGray
"     Black	White
"     Orange	Purple		Violet

augroup TempHighlight

autocmd ColorScheme,VimEnter * highlight TempHigh_0 guifg=White guibg=DarkRed    gui=NONE
autocmd ColorScheme,VimEnter * highlight TempHigh_1 guifg=White guibg=DarkBlue   gui=NONE
autocmd ColorScheme,VimEnter * highlight TempHigh_2 guifg=White guibg=DarkGreen  gui=NONE
autocmd ColorScheme,VimEnter * highlight TempHigh_3 guifg=Black guibg=LightRed   gui=NONE
autocmd ColorScheme,VimEnter * highlight TempHigh_4 guifg=Black guibg=LightBlue  gui=NONE
autocmd ColorScheme,VimEnter * highlight TempHigh_5 guifg=Black guibg=LightGreen gui=NONE
autocmd ColorScheme,VimEnter * highlight TempHigh_6 guifg=Black guibg=Yellow     gui=NONE
autocmd ColorScheme,VimEnter * highlight TempHigh_7 guifg=Black guibg=Cyan       gui=NONE
autocmd ColorScheme,VimEnter * highlight TempHigh_8 guifg=White guibg=Gray       gui=NONE
autocmd ColorScheme,VimEnter * highlight TempHigh_9 guifg=White guibg=DarkCyan   gui=NONE

augroup END

let s:active_highlights = {}

" Add temporary highlight, overwrite if index already used
function! TempHighlight_Add(index, regex)
    if a:index !~ "^[0-9]$"
        echoerr "Invalid index:" a:index
        return
    endif

    if has_key(s:active_highlights, a:index)
        call matchdelete(s:active_highlights[a:index][0])
    endif

    let s:active_highlights[a:index] = [ matchadd(printf("TempHigh_%s", a:index), a:regex), a:regex ]
endfunction

" Remove temporary highlight
function! TempHighlight_Del(index)
    if a:index !~ "^[0-9]$"
        echo printf("Index %d not used", a:index)
        return
    endif

    if ! has_key(s:active_highlights, a:index)
        return
    endif

    call matchdelete(s:active_highlights[a:index][0])
    call remove(s:active_highlights, a:index)
endfunction

" List all active temporary highlights
function! TempHighlight_List()
    if 0 == len(s:active_highlights)
        echo "No highlights active"
        return
    endif
    echo "Index: Regex"
    for [index, value] in items(s:active_highlights)
        execute printf("echohl TempHigh_%s", index)
        echo printf("%s    : %s", index, value[1])
        echohl None
    endfor
endfunction

" Clear all active temporary highlights
function! TempHighlight_Clear()
    for index in keys(s:active_highlights)
        call TempHighlight_Del(index)
    endfor
endfunction

" Query index and regular expression for TempHighlight_Add from user
function! TempHighlight_Add_interactive()
    call inputsave()
    let l:index = input("Highlight index: ")
    let l:regex = input("Highlight regular expression: ")
    call inputrestore()
    call TempHighlight_Add(l:index, l:regex)
endfunction

" Query index for TempHighlight_Del from user
function! TempHighlight_Del_interactive()
    call inputsave()
    let l:index = input("Highlight index: ")
    call inputrestore()
    call TempHighlight_Del(l:index)
endfunction

nnoremap <Leader>ha :call TempHighlight_Add_interactive()<CR>
nnoremap <Leader>hd :call TempHighlight_Del_interactive()<CR>
nnoremap <Leader>hl :call TempHighlight_List()<CR>
nnoremap <Leader>hc :call TempHighlight_Clear()<CR>
